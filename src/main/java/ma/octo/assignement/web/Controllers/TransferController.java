package ma.octo.assignement.web.Controllers;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.AuditTransferService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transfers")
class TransferController {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    @Autowired
    private CompteService compteService;
    @Autowired
    private TransferService transferService;
    @Autowired
    private AuditTransferService auditTransferService;
    @Autowired
    private final UtilisateurRepository utilisateurRepository;

    @Autowired
    TransferController(UtilisateurRepository utilisateurRepository) {
        this.utilisateurRepository = utilisateurRepository;
    }

    @GetMapping("/listDesTransferts")
    List<Transfer> loadAllTransfer() {
        LOGGER.info("Lister des utilisateurs");
        var all = transferService.loadAllTransfer();
        if (CollectionUtils.isEmpty(all)) { return null;}
        else { return CollectionUtils.isEmpty(all) ? all : null; }
    }

    @PostMapping("/executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        transferService.createTransaction(transferDto,MONTANT_MAXIMAL);
        auditTransferService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                        .toString());
    }


}
