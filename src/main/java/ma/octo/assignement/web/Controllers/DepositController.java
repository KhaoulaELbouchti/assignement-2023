package ma.octo.assignement.web.Controllers;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.AuditDepositService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.DepositService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/deposits")
public class DepositController {

        public static final int MONTANT_MAXIMAL = 10000;

        Logger LOGGER = LoggerFactory.getLogger(DepositController.class);

        @Autowired
        private CompteService compteService;
        @Autowired
        private DepositService depositService;
        @Autowired
        private AuditDepositService auditDepositService;
        @Autowired
        private final UtilisateurRepository utilisateurRepository;

        @Autowired
        DepositController(UtilisateurRepository utilisateurRepository) {
        this.utilisateurRepository = utilisateurRepository;
    }

        @GetMapping("/listDesDeposits")
        List<Deposit> loadAllDeposit() {
        LOGGER.info("Lister des utilisateurs");
        var all = depositService.loadAllDeposit();
        if (CollectionUtils.isEmpty(all)) { return null;}
        else { return CollectionUtils.isEmpty(all) ? all : null; }
    }

        @PostMapping("/executerDeposits")
        @ResponseStatus(HttpStatus.CREATED)
        public void createDeposit(@RequestBody DepositDto depositDto)
            throws CompteNonExistantException, TransactionException {
            depositService.createDeposit(depositDto,MONTANT_MAXIMAL);
            auditDepositService.auditDeposit("Deposit vers le compte ayant le RIB" + depositDto
                    .getRibCompteBeneficiaire() + " d'un montant de " + depositDto.getMontant()
                    .toString());
    }


    }
