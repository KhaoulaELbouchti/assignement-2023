package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "Deposit")
public class Deposit {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long idDeposit;

  @Column(precision = 16, scale = 2, nullable = false)
  private BigDecimal Montant;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateExecution;

  @Column
  private String nom_prenom_emetteur;

  @Column
  private String ribCompteBeneficiaire;

  @Column(length = 200)
  private String motifDeposit;
}
