package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.domain.util.EventType;

import javax.persistence.*;
@Getter
@Setter
@Entity
@Table(name = "AUDIT_Deposit")
public class AuditDeposit {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long idAuditDeposit;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;

}
