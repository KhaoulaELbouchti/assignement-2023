package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.domain.util.EventType;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
@Getter
@Setter
public class AuditDepositDto {
    private Long idAuditDeposit;
    private String message;
    private EventType eventType;
}
