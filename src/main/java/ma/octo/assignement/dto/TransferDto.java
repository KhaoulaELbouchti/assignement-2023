package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.domain.Compte;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class TransferDto {
  private Long idTransfer;
  private String nrCompteEmetteur;
  private String nrCompteBeneficiaire;
  private String motif;
  private BigDecimal montant;
  private Date date;

}
