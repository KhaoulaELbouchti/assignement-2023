package ma.octo.assignement.repository;

import ma.octo.assignement.domain.AuditDeposit;
import org.springframework.data.jpa.repository.JpaRepository;
public interface AuditDepositrepository extends JpaRepository<AuditDeposit, Long> {
}
