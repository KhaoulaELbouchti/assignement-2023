package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface DepositService {
    List<Deposit> loadAllDeposit();
    void createDeposit(DepositDto depositDto, int MONTANT_MAXIMAL)throws CompteNonExistantException, TransactionException;


}
