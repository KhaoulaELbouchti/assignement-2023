package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;

import java.math.BigDecimal;
import java.util.List;

public interface CompteService {
    Compte createCompte(Compte compte);
    List<Compte> loadAllCompte();
    void updateSoldeCompte(Compte compte, BigDecimal solde);
    Compte findByNrCompte(String nbrCompte) throws CompteNonExistantException;
    Compte findByRib(String rib) throws CompteNonExistantException;

}
