package ma.octo.assignement.service.Impl;


import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UtilisateurServiceImpl implements UtilisateurService {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Override
    public Utilisateur createUtilisateur(Utilisateur utilisateur){
        Utilisateur newUtilisateur = utilisateurRepository.save(utilisateur);
        return newUtilisateur;
    };

    @Override
    public List<Utilisateur> loadAllUtilisateur(){
        return  utilisateurRepository.findAll();
    };


}
