package ma.octo.assignement.service.Impl;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class DepositServiceImpl implements DepositService {


    @Autowired
    private DepositRepository depositRepository;
    @Autowired
    private CompteService compteService;

    @Override
    public List<Deposit> loadAllDeposit(){
        return  depositRepository.findAll();
    };

    @Override
    public void createDeposit(DepositDto depositDto, int MONTANT_MAXIMAL)
            throws CompteNonExistantException, TransactionException {

        Compte compteBeneficiaire = compteService.findByRib(depositDto.getRibCompteBeneficiaire());

        //gestion des exceptions des 3 cas possible pour le montant envoyé: vide,maximal
        if (depositDto.getMontant().equals(null) || depositDto.getMontant().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (depositDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de deposit dépassé");
            throw new TransactionException("Montant maximal de deposit dépassé");
        }

        //gestion d exception si l'utilisateur ne specifie pas le motif
        if (depositDto.getMotifDeposit().length() < 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        //modefier le solde du beneficiaire en ajoutant la somme envoyé
        compteService.updateSoldeCompte(compteBeneficiaire, new BigDecimal(compteBeneficiaire.getSolde().intValue() + depositDto.getMontant().intValue()));


        //enregistrer le  deposit
        Deposit deposit = new Deposit();
        deposit.setMontant(depositDto.getMontant());
        deposit.setDateExecution(depositDto.getDateExecution());
        deposit.setNom_prenom_emetteur(depositDto.getNom_prenom_emetteur());
        deposit.setMotifDeposit(depositDto.getMotifDeposit());
        deposit.setRibCompteBeneficiaire(depositDto.getRibCompteBeneficiaire());
        depositRepository.save(deposit);

    }
}
