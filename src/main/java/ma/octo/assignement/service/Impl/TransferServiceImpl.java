package ma.octo.assignement.service.Impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class TransferServiceImpl implements TransferService {

    @Autowired
    private TransferRepository transferRepository;
    @Autowired
    private CompteService compteService;

    @Override
    public List<Transfer> loadAllTransfer(){
        return  transferRepository.findAll();
    };

    @Override
    public void createTransaction(TransferDto transferDto,int MONTANT_MAXIMAL)
        throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

            //la premiere chose c'est de s'assurer que les comptes existent deja et dans ce cas recuperer les nbr
            Compte compteEmetteur = compteService.findByNrCompte(transferDto.getNrCompteEmetteur());
            Compte compteBeneficiaire = compteService.findByNrCompte(transferDto.getNrCompteBeneficiaire());

            //gestion des exceptions des 3 cas possible pour le montant: vide, minimal,maximal
            if (transferDto.getMontant().equals(null) || transferDto.getMontant().intValue() == 0) {
                System.out.println("Montant vide");
                throw new TransactionException("Montant vide");
            } else if (transferDto.getMontant().intValue() < 10) {
                System.out.println("Montant minimal de transfer non atteint");
                throw new TransactionException("Montant minimal de transfer non atteint");
            } else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
                System.out.println("Montant maximal de transfer dépassé");
                throw new TransactionException("Montant maximal de transfer dépassé");
            }

            //gestion d exception si l'utilisateur ne specifie pas le motif
            if (transferDto.getMotif().length() < 0) {
                System.out.println("Motif vide");
                throw new TransactionException("Motif vide");
            }

            //gestion d exception si le Solde est insuffisant
            if (compteEmetteur.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
                System.out.println("Solde insuffisant pour l'utilisateur");
                throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
            }

            //modefier le solde de l'emetteur
            compteService.updateSoldeCompte(compteEmetteur, compteEmetteur.getSolde().subtract(transferDto.getMontant()));

            //modefier le solde du recepteur en ajoutant la somme envoyé
            compteService.updateSoldeCompte(compteBeneficiaire, new BigDecimal(compteBeneficiaire.getSolde().intValue() + transferDto.getMontant().intValue()));


            //enregistrer le transfer
            Transfer transfer = new Transfer();
            transfer.setDateExecution(transferDto.getDate());
            transfer.setCompteBeneficiaire(compteBeneficiaire);
            transfer.setCompteEmetteur(compteEmetteur);
            transfer.setMontantTransfer(transferDto.getMontant());
            transferRepository.save(transfer);


    }

}
