package ma.octo.assignement.service.Impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.CompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CompteServiceImpl implements CompteService {

    @Autowired
    private CompteRepository compteRepository;

    @Override
    public Compte createCompte(Compte compte){
        Compte newCompte = compteRepository.save(compte);
        return newCompte;
    }

    @Override
    public List<Compte> loadAllCompte(){
        return  compteRepository.findAll();
    }

    @Override
    public Compte findByNrCompte(String nbrCompte) throws CompteNonExistantException{
        Compte compte = compteRepository.findByNrCompte(nbrCompte);
        if (compte == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant"); }
        return compte;
    }
    @Override
    public void updateSoldeCompte(Compte compte, BigDecimal solde){
        Compte leCompte = compteRepository.findByNrCompte(compte.getNrCompte());
        leCompte.setSolde(solde);
        compteRepository.save(leCompte);
    }

    @Override
    public Compte findByRib(String rib) throws CompteNonExistantException{
        Compte compte = compteRepository.findByRib(rib);
        if (compte == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant"); }
        return compte;
    }


}
