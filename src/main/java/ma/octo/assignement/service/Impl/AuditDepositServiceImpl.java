package ma.octo.assignement.service.Impl;

import ma.octo.assignement.domain.AuditTransfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditTransferRepository;
import ma.octo.assignement.service.AuditDepositService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditDepositServiceImpl implements AuditDepositService {

    Logger LOGGER = LoggerFactory.getLogger(AuditDepositServiceImpl.class);

    @Autowired
    private AuditTransferRepository auditTransferRepository;

    @Override
    public void auditDeposit(String message) {
        LOGGER.info("Audit de l'événement {}", EventType.DEPOSIT);
        AuditTransfer audit = new AuditTransfer();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage(message);
        auditTransferRepository.save(audit);
    }

}
