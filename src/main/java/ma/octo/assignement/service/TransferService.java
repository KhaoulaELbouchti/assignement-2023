package ma.octo.assignement.service;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.math.BigDecimal;
import java.util.List;

public interface TransferService {

    List<Transfer> loadAllTransfer();
    void createTransaction(TransferDto transferDto, int MONTANT_MAXIMAL)throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;


}
