package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;

import java.util.List;

public interface UtilisateurService {

    Utilisateur createUtilisateur(Utilisateur utilisateur);

    List<Utilisateur> loadAllUtilisateur();
}
