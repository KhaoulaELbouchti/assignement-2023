package ma.octo.assignement.web.Controllers;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.Impl.CompteServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CompteController.class)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = CompteController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CompteControllerTest {


    private Compte compte = new Compte();
    private List<Compte> comptes = new ArrayList<Compte>();
    private Utilisateur user1=new Utilisateur();
    private Utilisateur user2=new Utilisateur();


    @Autowired
    MockMvc mvc;

    @MockBean
    private CompteServiceImpl compteService;
    //



    @MockBean
    CompteRepository compteRepository;

//
    @BeforeAll
    void initData() {
        Compte compte1 = new Compte();
        compte1.setIdCompte(1L);
        compte1.setNrCompte("1");
        compte1.setRib("1234");
        compte1.setSolde(new BigDecimal(120));
        compte1.setUtilisateur(user1);

        Compte compte2 = new Compte();
        compte2.setIdCompte(2L);
        compte2.setNrCompte("2");
        compte2.setRib("1233");
        compte2.setSolde(new BigDecimal(120));
        compte2.setUtilisateur(user2);


        comptes.add(compte1);
        comptes.add(compte2);
    }
    @Test
    void should_Get_Empty_List_Compte() throws Exception {

        //
        // When
        //
        Mockito.when(compteService.loadAllCompte()).thenReturn(new ArrayList<>());
        RequestBuilder reqBuilder = MockMvcRequestBuilders.get("/comptes/listOfAccounts");
        MvcResult result = mvc.perform(reqBuilder).andExpect(status().isOk()).andReturn();
        List<Compte> response= new ArrayList<>();
        response = compteService.loadAllCompte();
        // Then
        assertTrue(response.isEmpty());
        assertEquals(0, response.toArray().length);
    }
    @Test
    void loadAllCompte() throws Exception{
        //
        // When
        //
        Mockito.when(compteService.loadAllCompte()).thenReturn(comptes);
        RequestBuilder reqBuilder = MockMvcRequestBuilders.get("/comptes/listOfAccounts");

        MvcResult result = mvc.perform(reqBuilder).andExpect(status().isOk()).andReturn();
        List<Compte> response= new ArrayList<>();
        response = compteService.loadAllCompte();
        // Then
        assertEquals(1L, response.get(0).getIdCompte());
        assertEquals(2, response.toArray().length);
    }
}