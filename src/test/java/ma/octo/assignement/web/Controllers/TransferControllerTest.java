package ma.octo.assignement.web.Controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.ObjectMapperUtils;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.AuditTransferService;
import ma.octo.assignement.service.Impl.CompteServiceImpl;
import ma.octo.assignement.service.Impl.TransferServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = TransferController.class)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TransferController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TransferControllerTest {


    private Transfer transfer = new Transfer();
    private List<Transfer> transfers = new ArrayList<Transfer>();
    private TransferDto requestDto = new TransferDto();
    private Compte compte1 = new Compte();
    private Compte compte2 = new Compte();
    private Utilisateur user1=new Utilisateur();
    private Utilisateur user2=new Utilisateur();

    @Autowired
    MockMvc mvc;

    @MockBean
    private TransferServiceImpl transferService;

    @MockBean
    TransferRepository transferRepository;

    @MockBean
    UtilisateurRepository utilisateurRepository;

    @MockBean
    private CompteServiceImpl compteService;

    @MockBean
    private AuditTransferService auditTransferService;

    @BeforeAll
    void initData() {
        compte1.setIdCompte(1L);
        compte1.setNrCompte("1");
        compte1.setRib("1234");
        compte1.setSolde(new BigDecimal(120));
        compte1.setUtilisateur(user1);

        compte2.setIdCompte(2L);
        compte2.setNrCompte("2");
        compte2.setRib("1233");
        compte2.setSolde(new BigDecimal(120));
        compte2.setUtilisateur(user2);

        Transfer transfer1 = new Transfer();
        transfer1.setIdTransfer(1L);
        transfer1.setMontantTransfer(new BigDecimal(20));
        transfer1.setDateExecution(new Date());
        transfer1.setCompteBeneficiaire(compte1);
        transfer1.setCompteEmetteur(compte2);
        transfer1.setMotifTransfer("dh");


        Transfer transfer2 = new Transfer();
        transfer2.setIdTransfer(2L);
        transfer2.setMontantTransfer(new BigDecimal(20));
        transfer2.setDateExecution(new Date());
        transfer2.setCompteBeneficiaire(compte1);
        transfer2.setCompteEmetteur(compte2);
        transfer2.setMotifTransfer("dh");

        requestDto.setIdTransfer(3L);
        requestDto.setMontant(new BigDecimal(20));
        requestDto.setDate(new Date());
        requestDto.setNrCompteEmetteur(compte1.getNrCompte());
        requestDto.setNrCompteBeneficiaire(compte2.getNrCompte());
        requestDto.setMotif("dh");


        transfers.add(transfer1);
        transfers.add(transfer2);
    }
    @Test
    void should_Get_Empty_List_Transfer() throws Exception {

        //
        // When
        //
        Mockito.when(transferService.loadAllTransfer()).thenReturn(new ArrayList<>());
        RequestBuilder reqBuilder = MockMvcRequestBuilders.get("/transfers/listDesTransferts");
        MvcResult result = mvc.perform(reqBuilder).andExpect(status().isOk()).andReturn();
        List<Transfer> response= new ArrayList<>();
        response = transferService.loadAllTransfer();
        // Then
        assertTrue(response.isEmpty());
        assertEquals(0, response.toArray().length);
    }


    @Test
    void loadAllTransfer() throws Exception{
        //
        // When
        //
        Mockito.when(transferService.loadAllTransfer()).thenReturn(transfers);
        RequestBuilder reqBuilder = MockMvcRequestBuilders.get("/transfers/listDesTransferts");

        MvcResult result = mvc.perform(reqBuilder).andExpect(status().isOk()).andReturn();
        List<Transfer> response= new ArrayList<>();
        response = transferService.loadAllTransfer();
        // Then
        assertFalse(response.isEmpty());
        assertEquals(2L, response.get(1).getIdTransfer());
        assertEquals(2, response.toArray().length);
    }

    @Test
    void should_Return_Bad_Request_When_Body_Is_Empty() throws Exception {

        //
        // When
        //

        RequestBuilder reqBuilder = MockMvcRequestBuilders.post("/transfers/executerTransfers")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

        //
        // then
        //

        mvc.perform(reqBuilder).andExpect(status().isBadRequest());

    }

    @Test
    void createTransaction() throws Exception{
        String requestJson = ObjectMapperUtils.mapToJson(requestDto);
        RequestBuilder reqBuilder = MockMvcRequestBuilders.post("/transfers/executerTransfers")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(requestJson);
        MvcResult result = mvc.perform(reqBuilder).andExpect(status().isCreated()).andReturn();
    }
}