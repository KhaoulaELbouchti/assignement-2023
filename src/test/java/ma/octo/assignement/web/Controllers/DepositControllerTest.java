package ma.octo.assignement.web.Controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.util.ObjectMapperUtils;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.AuditDepositService;
import ma.octo.assignement.service.Impl.CompteServiceImpl;
import ma.octo.assignement.service.Impl.DepositServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = DepositController.class)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DepositController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DepositControllerTest {


    private Deposit deposit = new Deposit();
    private List<Deposit> deposits = new ArrayList<Deposit>();
    private DepositDto requestDto = new DepositDto();


    @Autowired
    MockMvc mvc;

    @MockBean
    private DepositServiceImpl depositService;

    @MockBean
    DepositRepository depositRepository;

    @MockBean
    UtilisateurRepository utilisateurRepository;

    @MockBean
    private CompteServiceImpl compteService;

    @MockBean
    private AuditDepositService auditDepositService;

    @BeforeAll
    void initData() {
        Deposit deposit1 = new Deposit();
        deposit1.setIdDeposit(1L);
        deposit1.setMontant(new BigDecimal(200));
        deposit1.setDateExecution(new Date());
        deposit1.setNom_prenom_emetteur("nom1");
        deposit1.setRibCompteBeneficiaire("1231");
        deposit1.setMotifDeposit("dh");

        Deposit deposit2 = new Deposit();
        deposit2.setIdDeposit(2L);
        deposit2.setMontant(new BigDecimal(200));
        deposit2.setDateExecution(null);
        deposit2.setNom_prenom_emetteur("nom2");
        deposit2.setRibCompteBeneficiaire("1232");
        deposit2.setMotifDeposit("dh");

        requestDto.setIdDeposit(3L);
        requestDto.setMontant(new BigDecimal(200));
        requestDto.setDateExecution(null);
        requestDto.setNom_prenom_emetteur("nom3");
        requestDto.setRibCompteBeneficiaire("1233");
        requestDto.setMotifDeposit("dh");

        deposits.add(deposit1);
        deposits.add(deposit2);
    }
    @Test
    void should_Get_Empty_List_Deposit() throws Exception {

        //
        // When
        //
        Mockito.when(depositService.loadAllDeposit()).thenReturn(new ArrayList<>());
        RequestBuilder reqBuilder = MockMvcRequestBuilders.get("/deposits/listDesDeposits");
        MvcResult result = mvc.perform(reqBuilder).andExpect(status().isOk()).andReturn();
        List<Deposit> response= new ArrayList<>();
        response = depositService.loadAllDeposit();
        // Then
        assertTrue(response.isEmpty());
        assertEquals(0, response.toArray().length);
    }
    @Test
    void loadAllDeposit() throws Exception{

        // When
        //
        Mockito.when(depositService.loadAllDeposit()).thenReturn(deposits);
        RequestBuilder reqBuilder = MockMvcRequestBuilders.get("/deposits/listDesDeposits");

        MvcResult result = mvc.perform(reqBuilder).andExpect(status().isOk()).andReturn();
        List<Deposit> response= new ArrayList<>();
        response = depositService.loadAllDeposit();
        // Then
        assertEquals(1L, response.get(0).getIdDeposit());
        assertEquals(2, response.toArray().length);
    }
    @Test
    void should_Return_Bad_Request_When_Body_Is_Empty() throws Exception {

        //
        // When
        //

        RequestBuilder reqBuilder = MockMvcRequestBuilders.post("/deposits/executerDeposits")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

        //
        // then
        //

        mvc.perform(reqBuilder).andExpect(status().isBadRequest());

    }
    @Test
    void createDeposit() throws Exception {

        Deposit deposit3 = new Deposit();
        deposit3.setIdDeposit(3L);
        deposit3.setMontant(new BigDecimal(200));
        deposit3.setDateExecution(requestDto.getDateExecution());
        deposit3.setNom_prenom_emetteur("nom3");
        deposit3.setRibCompteBeneficiaire("1233");
        deposit3.setMotifDeposit("dh");

        String requestJson = ObjectMapperUtils.mapToJson(requestDto);
        RequestBuilder reqBuilder = MockMvcRequestBuilders.post("/deposits/executerDeposits")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(requestJson);
        MvcResult result = mvc.perform(reqBuilder).andExpect(status().isCreated()).andReturn();

    }
}