package ma.octo.assignement.web.Controllers;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.Impl.UtilisateurServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UtilisateurController.class)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = UtilisateurController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UtilisateurControllerTest {


    private List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
    private Utilisateur user=new Utilisateur();


    @Autowired
    MockMvc mvc;

    @MockBean
    private UtilisateurServiceImpl utilisateurService;
    //



    @MockBean
    UtilisateurRepository utilisateurRepository;

//
    @BeforeAll
    void initData() {
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setIdUtilisateur(1L);

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setIdUtilisateur(2L);


        utilisateurs.add(utilisateur1);
        utilisateurs.add(utilisateur2);
    }

    @Test
    void should_Get_Empty_List_Compte() throws Exception {

        //
        // When
        //
        Mockito.when(utilisateurService.loadAllUtilisateur()).thenReturn(new ArrayList<>());
        RequestBuilder reqBuilder = MockMvcRequestBuilders.get("/utilisateurs/lister_utilisateurs");
        MvcResult result = mvc.perform(reqBuilder).andExpect(status().isOk()).andReturn();
        List<Utilisateur> response= new ArrayList<>();
        response = utilisateurService.loadAllUtilisateur();
        // Then
        assertTrue(response.isEmpty());
        assertEquals(0, response.toArray().length);
    }
    @Test
    void loadAllUtilisateur() throws Exception{


        // When
        //
        Mockito.when(utilisateurService.loadAllUtilisateur()).thenReturn(utilisateurs);
        RequestBuilder reqBuilder = MockMvcRequestBuilders.get("/utilisateurs/lister_utilisateurs");

        MvcResult result = mvc.perform(reqBuilder).andExpect(status().isOk()).andReturn();
        List<Utilisateur> response= new ArrayList<>();
        response = utilisateurService.loadAllUtilisateur();
        // Then
        assertEquals(1L, response.get(0).getIdUtilisateur());
        assertEquals(2, response.toArray().length);
    }
}